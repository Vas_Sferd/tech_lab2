/*
 * main.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*
 * Лабораторная работа 2
 * Поиск с использованием единого справочника
 * Вариант 20
 * 
 * */

#define NDEBUG

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "types.h" 

#ifndef NDEBUG
    #define DBG() printf("[I] (%s:%d)\n", __FILE__, __LINE__)
#else
    #define DBG()
#endif

// Функции сравнения для сортировки
typedef int (* fcomp_t)(const void *, const void *);

int dataCompareByKey1(const data_t * first, const data_t * second) {	return first->key[0] > second->key[0]; }
int dataCompareByKey2(const data_t * first, const data_t * second) {	return first->key[1] > second->key[1]; }
int dataCompareByKey3(const data_t * first, const data_t * second) {	return first->key[2] > second->key[2]; }

const fcomp_t dataCompareByKey[3] = {(fcomp_t)dataCompareByKey1, (fcomp_t)dataCompareByKey2, (fcomp_t)dataCompareByKey3};

// Функции предикатор для поиска
typedef int (* fpredicat_t)(const data_t *, float);

int dataPredicat1(const data_t * data, float key) { return data->key[0] == key; }
int dataPredicat2(const data_t * data, float key) { return data->key[1] == key; }
int dataPredicat3(const data_t * data, float key) { return data->key[2] == key; }

const fpredicat_t dataPredicat[3] = {dataPredicat1, dataPredicat2, dataPredicat3};

// Генерация некоторых данных
data_t * genDataArray(int count)
{
    DBG();

    data_t * source = calloc(count, sizeof(data_t));
	
    for (int i = 0; i < count; ++i)
	{
        DBG();

        for (size_t j = 0; j < 3; ++j)
		{
            source[i].key[j]		= (float)(rand() % 8) / 1.0;
            source[i].vector1[j]	= rand() % 128;
            source[i].vector2[j]	= rand() % 128;
		}
		
        source[i].type = rand() % ('z' - 'a' + 1) + 'a';

        DBG();
	}
	
    DBG();

	return source;
}

// Поиск по ключу
data_t * refFind(unified_ref_t * ref, size_t * count, float key, fpredicat_t predicat)
{
    // Результирующий массив
    data_t * filtered = calloc(*count, sizeof(data_t));

    DBG();

    size_t new_count = 0;
    for (size_t i = 0; i < ref->blocks_count; ++i)
    {
        if (ref->blocks[i].key <= key)
        {
            size_t last_index = ref->blocks[i].count;
            size_t old_count = new_count;

            DBG();

            // Проверяем последние элементы в блоке
            for (int j = (int)last_index; j >= 0; --j)
            {
                if (predicat(&ref->blocks[i].data[j], key))
                {
                    // Добавляем элемент в результирующий массив
                    memcpy(&filtered[new_count], &(ref->blocks[i].data[j]), sizeof(data_t));
                    ++new_count;    // Двигаемся дальше
                }
                // Выход, если предыдущий соответствовал
                else if (new_count != old_count)
                {
                    break;  // Завершае обход данного блока
                }
            }
        }

        DBG();
    }

    *count = new_count;                         // Обновляем размер массива данных
    //filtered = realloc(filtered, new_count);    // Меняем размер данных массива
    assert(filtered);

    return filtered;
}

// Создание общего справочника
unified_ref_t * mkRef(const data_t * data, size_t count)
{
    unified_ref_t * ref = malloc(sizeof(unified_ref_t));
    
    if (ref)
    {
        // Подсчитываем размеры блоков и их число
        ref->blocks_count = floor(sqrt(count));         // Число блоков
        size_t in_block_count = ref->blocks_count + 1;  // Число элементов в блоке

        // Добавляем недостающий блок если необходимо
        if (count > in_block_count * ref->blocks_count)
        {
            ++ref->blocks_count;
        }

        // Выделяем память
        ref->blocks = calloc(ref->blocks_count, sizeof(*ref->blocks));

        if (ref->blocks)
        {
            // Обходим каждый блок
            for (int i = 0; i < (int)ref->blocks_count; ++i)
            {
                int data_index = i * in_block_count;

                ref->blocks[i].count = in_block_count;
                ref->blocks[i].data = &data[data_index];
            }

            // Для последней нецелого блока
            if (ref->blocks_count * in_block_count > count)
            {
                size_t last_i = ref->blocks_count - 1;                  // Индекс последнего элемента
                ref->blocks[last_i].count = count % in_block_count;     // Число элементов в последнем блоке
            }
        }
    }
    
    DBG();

    return ref;
}

int printData(const data_t * data, size_t count)
{
    for (size_t i = 0; i < count; ++i)
    {
        printf(
                "%zu)\n"
                "\tkey: %.0f, %.0f, %.0f\n"
                "\tvector1: %d, %d, %d\n"
                "\tvector2: %d, %d, %d\n"
                "\ttype: %c;\n"

                , i + 1,
                data[i].key[0], data[i].key[1], data[i].key[2],
                data[i].vector1[0], data[i].vector1[1], data[i].vector1[2],
                data[i].vector2[0], data[i].vector2[1], data[i].vector2[2],
                data[i].type
                );
    }

    putchar('\n');

    return 0;
}

int printRef(unified_ref_t * ref)
{
    size_t count = 0;
    for (size_t i = 0; i < ref->blocks_count; ++i)
    {
        count += ref->blocks[i].count;
    }

    printData(ref->blocks->data, count);

    return 0;
}

// Точка входа
int main(int argc, char **argv)
{
    srand(time(0));

    float norm[3];                                                      // Критерии фильтрации
    int source_count = 0;                                               // Число элементов в массиве данных
    int is_correct = (argc == 5 && (source_count = atoi(argv[1])) > 0); // Коррректность

    DBG();

    if (is_correct)
    {
        for (size_t i = 0; is_correct && i < 3; ++i)
        {
            char * endstr;
            norm[i] = strtof(argv[2 + i], &endstr);

            // Проверка на не корректность аргументов
            if (strcmp(argv[2 + i], endstr) == 0)
            {
                is_correct = 0;
            }
            assert(is_correct);
        }

        source_count = atoi(argv[1]);
        assert(source_count);
        DBG();
    }

    if (is_correct)   // Проверка на верные аргумент командной строки
    {
        // Генерируем данные
        data_t * source = genDataArray(source_count);
        assert(source);

        DBG();

        // Создаём массивы и словари для поэапной работы с данными
        data_t * arrays[3];
        unified_ref_t * refs[3];

        data_t * cur_array = calloc(source_count, sizeof(data_t));
        memcpy(cur_array, source, source_count * sizeof(data_t));
        assert(cur_array);
        size_t count = source_count;

        DBG();

        for (size_t i = 0; i < 3; ++i)
        {
            // Выбираем предыдущий результат или копию исходного массива
            arrays[i] = cur_array;

            // Сортируем по ключу
            qsort(arrays[i], count, sizeof(data_t), dataCompareByKey[i]);

            // Создаём новый единый справочники
            refs[i] = mkRef(arrays[i], count);
            assert(refs[i]);
            
            DBG();
            
            for (size_t j = 0; j < refs[i]->blocks_count; ++j)
            {
                // Заполняем значения ключей для каждого блока
                int index = refs[i]->blocks[j].count - 1;
                refs[i]->blocks[j].key = refs[i]->blocks[j].data[index].key[i];
            }

			DBG();
			
            // Поиск
            cur_array = refFind(refs[i], &count, norm[i], dataPredicat[i]);
            assert(cur_array);
        }

        DBG();

        // Вывод результата и очистка памяти
        puts("Generated Source:\n");
        printData(source, source_count);
        free(source);

        for (size_t i = 0; i < 3; ++i)
        {
            printf("Stage %zu:\n", i);
            printRef(refs[i]);

            free(arrays[i]);
            free(refs[i]->blocks);
            free(refs[i]);
        }

        printf("Result:\n");
        printData(cur_array, count);
        free(cur_array);

        DBG();

        puts("Finish");
        getchar();
    }
	else
	{
		perror("Invalid arguments\n");
	}
	
	return 0;
}

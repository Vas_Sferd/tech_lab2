/*
 * types.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _TYPES_H
#define _TYPES_H

#include <stdlib.h>

// Типы данных
typedef struct
{
	float key[3];
    int vector1[3];
	int vector2[3];
	char type;
	
} data_t;

typedef struct
{
	size_t blocks_count;
	struct
	{
		size_t count;
        const data_t * data;
		float key;
		
	} * blocks;

} unified_ref_t;

#endif
